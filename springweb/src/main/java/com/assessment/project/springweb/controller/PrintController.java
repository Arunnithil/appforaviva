package com.assessment.project.springweb.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import Exception.AppException;

import com.assessment.project.springweb.delegate.PrintDelegate;

@RestController
@RequestMapping(value="print", produces = MediaType.APPLICATION_JSON_VALUE)
public class PrintController {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(TestPrintController.class);
	
	@Autowired
	PrintDelegate delegate;
	
	@RequestMapping(value = "recordinput", method = RequestMethod.POST)
	@ResponseBody
	public  String recordInput(@PathVariable(value = "value") String value)  throws AppException{
		
		return delegate.recordInput(value);
	}
}
