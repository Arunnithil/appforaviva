package com.assessment.project.springweb.delegate;

import org.springframework.stereotype.Component;
import org.apache.log4j.Logger;

import Exception.AppException;

import com.assessment.project.springweb.controller.TestPrintController;

@Component
public class PrintDelegate {
	
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(TestPrintController.class);
	
	public String recordInput(String value) throws AppException{
		try{
			logger.info("Keyed in Value from User: "+value);
			return "SUCCESS";
		}catch(Exception e){
			throw new AppException();
		}
		
	}
}
