package Exception;

import org.springframework.http.HttpStatus;

public class AppException extends Exception{
	private static final long serialVersionUID = 1L;

	private int errorCode;

	private String errorMessage;

	public AppException(int errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public AppException() {
		super();
	}

	public AppException(HttpStatus status) {
		this(status.value(), status.getReasonPhrase());
	}

	public AppException(HttpStatus status, String errorMessage) {
		this(status.value(), errorMessage);
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public int getErrorCode() {
		return errorCode;
	}

}
