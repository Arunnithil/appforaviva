package util;



import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;


public class StringHelper {

	public static <T> String convertToJSon(T obj) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(obj);
	}
	
	public static <T> T convertToObject(String json, Class<T> clazz) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		//mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		//Object to JSON in String
		return mapper.readValue(json, clazz);
	}
	
	public static String convertToString(Map<String, String> map) {
		StringBuilder result = new StringBuilder(); 
		for(String key : map.keySet()) {
			result.append("{").append(key).append(":").append(map.get(key)).append("}");
		}
		return result.toString();
	}
	
	public static <Response> Response convertToObject(Gson gson, String responseBody, Class<Response> clazz) throws Exception {
		try {
			return gson.fromJson(responseBody, clazz);
		} catch (JsonSyntaxException jse) {
			throw new Exception(jse);
		}
	}
	
	public static <Response> List<Response> convertToListObject(String responseBody, Class<Response[]> clazz) throws Exception {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
//			Response[] mcArray = gson.fromJson(responseBody, clazz);
			Response[] mcArray = objectMapper.readValue(responseBody, clazz);
			return Arrays.asList(mcArray);
		} catch (IOException e) {
			throw new Exception(e);
		}
	}
}
