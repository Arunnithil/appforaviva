package util;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import Exception.AppException;



@Component
public class RestClient extends BaseResource {
	private Logger logger = Logger.getLogger(RestClient.class);
	private final Gson gson = new GsonBuilder().create();
	@Autowired
	private RestOperations restOperations;

	public RestClient() {
		if(restOperations == null) {
			restOperations = new RestTemplate();
		}
	}

	public RestClient(String host, int port) {
		// TODO Auto-generated constructor stub

		if(restOperations == null) {
			restOperations = new RestTemplate();
		}
	}


	public <Request, Response> Response post(String uri, Request request, Class<Response> clazz, Map<String, String> headerParams) throws Exception {
		return restCall(uri, request, clazz, headerParams, HttpMethod.POST);
	}

	public <Request, Response> Response post(String uri, Request requestBody, Class<Response> clazz) throws Exception {
		return post(uri, requestBody, clazz, null);
	}
	public String restCallDummyGetDummy(String uri, String requestBody, Map<String, String> headerParams, HttpMethod method) throws AppException {
		try {
			HttpEntity<String> requestEntity;
			if (requestBody != null && !requestBody.isEmpty()) {
				requestEntity = createHttpEntity(requestBody, headerParams);
			} else {
				requestEntity = createHttpEntity(headerParams);
			}
			ResponseEntity<String> responseEntity = restOperations.exchange(uri, method, requestEntity, String.class);
			return responseEntity.getBody();
		} catch (HttpStatusCodeException e){
			throw new AppException(e.getStatusCode(), e.getResponseBodyAsString());
		} catch (RestClientException rce) {
			throw new AppException();
		}


	}
	private HttpEntity<String> createHttpEntity(Map<String, String> headerParams) {
		Map<String, String> headers = headerParams;
		if (headerParams == null || headerParams.isEmpty()) {
			headers = new HashMap<String, String>();
		}
		return new HttpEntity<String>(ClientHelper.getHttpHeaders(headers));

	}

	private HttpEntity<String> createHttpEntity(String requestBody, Map<String, String> headerParams) {
		Map<String, String> headers = headerParams;
		if (headerParams == null || headerParams.isEmpty()) {
			headers = new HashMap<String, String>();
		}
		return new HttpEntity<String>(requestBody, ClientHelper.getHttpHeaders(headers));

	}
	public <Request, Response> Response restCall(String uri, Request request, Class<Response> clazz, Map<String, String> headerParams, HttpMethod method) throws Exception {
		try {
			String responseBody = restCallDummyGetDummy(uri, this.gson.toJson(request), headerParams, method);
			return StringHelper.convertToObject(gson, responseBody, clazz);
		} catch (RestClientException rce) {
			throw new AppException();
		}
	}

	public String postDummyGetDummy(String uri, String requestBody) throws AppException {
		return this.postDummyGetDummy(uri, requestBody, null);
	}

	public String postDummyGetDummy(String uri, String requestBody, Map<String, String> headerParams) throws AppException {
		return restCallDummyGetDummy(uri, requestBody, headerParams, HttpMethod.POST);
	}
}