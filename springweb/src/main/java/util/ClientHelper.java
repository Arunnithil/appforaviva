package util;

import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class ClientHelper {

	public static HttpHeaders getHttpHeaders(Map<String, String> headerParams) {
		headerParams.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headerParams.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		final HttpHeaders headers = new HttpHeaders();
		if (headerParams != null) {
			for (String key : headerParams.keySet()) {
				String value = headerParams.get(key);
				if (value != null) {
					headers.set(key, headerParams.get(key));
				}
			}
		}
		return headers;
	}
	public static String toServerUrl(String server, int port) {
		return "http://" + server + ":" + port;
	}
	
	public static String toServerUrl(String server, String port) {
		return "http://" + server + ":" + port;
	}
}
