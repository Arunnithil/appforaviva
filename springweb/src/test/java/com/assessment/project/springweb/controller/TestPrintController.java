package com.assessment.project.springweb.controller;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import util.RestClient;
import Exception.AppException;
import org.junit.Test;
import org.springframework.test.context.web.WebAppConfiguration;

@WebAppConfiguration
public class TestPrintController {
	private final String PATH = "/print/recordinput";
	private  final RestClient REST_CLIENT_API = new RestClient("localhost", 7001);
	
	@Test
	public void testUploadContractDocuments() throws AppException {
		String request = "{\"value\":\"3\"}";
		String response = REST_CLIENT_API.postDummyGetDummy(PATH , request);
		assertNotNull(response);
		assertTrue(!response.isEmpty());
	}

}
