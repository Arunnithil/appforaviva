package com.assignment.app;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.assignment.app.helper.MyHelper;
import com.assignment.app.helper.RestHelper;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	MyHelper helper;
	
	@Autowired
	RestHelper restHelper;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(@RequestParam("number") String number, Locale locale, Model model) {
		logger.info("Enter Positive integer {}.", locale);
		
	
		String result = helper.returvalues(number);
		if(result!=null){
		model.addAttribute("serverTime", result );
		restHelper.recordValues(number);
		}
		return "home";
	}
	
}
