package com.assignment.app.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.assignment.app.HomeController;


@Component
public class RestHelper {
	private final String PATH = "http://localhost:7001/print/recordinput";
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	public void recordValues(String input){
		 RestTemplate restTemplate = new RestTemplate();
	     ResponseEntity<String> res = restTemplate.getForEntity(PATH, String.class);
	     logger.info(res.toString());
	}
	
}
