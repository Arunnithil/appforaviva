package com.assignment.app.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.assignment.app.HomeController;
@Component
public class MyHelper {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	public String returvalues(String  inputVal){
			String output = "";
			int val = Integer.parseInt(inputVal);
			if(val>0 && val<1000){
				for(int i =1;i<=val;i++){
					output += ((i%3==0 && i%5==0) ? "FIZBUZZ " : i%3==0 ? "FIZZ " : i%5==0 ? "BUZZ "  : i+" ");
					output += "<br>";
					logger.info(output); 
					
				}
			}
			return output+"<br>";
	}
}
